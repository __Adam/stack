import { Stack } from './stack.service';

// funkcja describe tworzy nazwany zbiór testów jasmine
describe('Stack test', () => {
    // deklaracje np. pól wykorzystywanych w dalszych testach
    let stack: Stack;

    // pozwala ustawić pewien stan lub poczynić przygotowania przed każdym testem
    beforeEach(() => {
        stack = new Stack();
        stack.push('bbbb');
    });

    // funkcja it jest nazwaną definicją pojedynczego testu
    it('should push element on stack', () => {
        stack.push('aaaa');
        // elementy expect pozwalają tworzyć asercje/zapewnienia, po których spełnieniu możemy stwierdzić że test zakończył się bez błędu
        expect(stack.size()).toEqual(2);
    });

    it('should pop element from stack', () => {
        const result = stack.pop();
        expect(result).toEqual('bbbb');
    });


    it('should get all elements', () => {
        const result = stack.elements();
        expect(result).toEqual(['bbbb']);
    });


    it('should check stack size', () => {
        const result = stack.size();
        expect(result).toEqual(1);
    });
});
